import yaml
import os.path
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
file = "apps.yaml"
final_path = os.path.join(BASE_DIR, file)

with open(final_path,'r') as f:
    my_dict = yaml.safe_load(f)

print(my_dict['apps']['front'])